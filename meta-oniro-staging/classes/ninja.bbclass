# SPDX-FileCopyrightText: Huawei Inc.
#
# SPDX-License-Identifier: Apache-2.0

DEPENDS:prepend = "ninja-native "

MAXLOAD_NINJA ??= "0"
export MAXLOAD_NINJA
